//
//  Extensions.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/30/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit

extension UIButton {

	func roundCorners() {
		layer.cornerRadius = bounds.size.height / 2
		layer.masksToBounds = false
	}

	func setTitleForButton (_ name: String) {
		setTitle(name, for: .normal)
	}
}

extension UIViewController {

	func hideKeyboardWhenTappedAround() {
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
																 action: #selector(UIViewController.dismissKeyboardWithTap))
		tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
	}

	@objc func dismissKeyboardWithTap() {
		view.endEditing(true)
	}
}
