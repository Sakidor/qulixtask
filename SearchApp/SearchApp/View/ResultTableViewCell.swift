//
//  ResultTableViewCell.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/29/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {
	@IBOutlet private weak var resultLinkLabel: UILabel!

	func update(item: ResultItem) {
		resultLinkLabel.text = item.link
	}
}
