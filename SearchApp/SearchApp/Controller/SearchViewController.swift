//
//  ViewController.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/29/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit
import Speech
import SafariServices
import Toast_Swift

class SearchViewController: UIViewController, SFSpeechRecognizerDelegate {

	// MARK: - Outlets
	@IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet private weak var searchBar: UISearchBar!
	@IBOutlet private weak var myTableView: UITableView!
	@IBOutlet private weak var searchButton: UIButton!
	@IBOutlet private weak var microphoneButton: UIButton!

	private var searchResults: [ResultItem] = []

	@objc func dismissKeyboard() {
		searchBar.resignFirstResponder()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		searchButton.roundCorners()
		hideKeyboardWhenTappedAround()
		let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
		microphoneButton.addGestureRecognizer(longGesture)
		DataManager.shared.delegate = self
	}

	func doSpeechRecognize() {
	 SpeechManager.shared.recordAndRecognizeSpeech { [weak self] (result) in
			self?.searchBar.text = result
		}
	}

	@IBAction func searchButtonClicked(_ sender: UIButton) {
		dismissKeyboard()
		if !searchResults.isEmpty {
			searchResults = []
			myTableView.reloadData()
		}
		doSearch()
		if sender.titleLabel?.text == NSLocalizedString("Stop", comment: "") {
			DataManager.shared.stopSearch()
		}
	}

	func doSearch() {
		guard let text = searchBar.text, !text.isEmpty else {
			return
		}

		DataManager.shared.performSearch(keyWord: searchBar.text!) { [weak self] (result) in
			guard let strongSelf = self else {
				return
			}
			switch result {
			case .success(let data):
				strongSelf.searchResults = data
			case .mistake(let message):
				strongSelf.view.makeToast(message, duration: 1.0, position: .center)
			}
			strongSelf.myTableView.reloadData()
			strongSelf.activityIndicator.stopAnimating()
			strongSelf.searchButton.setTitleForButton(NSLocalizedString("Google Search", comment: ""))
		}
	}

	@objc func longTap(_ sender: UIGestureRecognizer) {

		print("Long tap")
		if sender.state == .ended {
			print("UIGestureRecognizerStateEnded")
			SpeechManager.shared.stopSpeechRecognizing()
			microphoneButton.setImage(UIImage(named: "microphone"), for: .normal)
			self.microphoneButton.transform = CGAffineTransform.identity
		} else if sender.state == .began {
			UIView.animate(withDuration: 0, animations: {
				self.microphoneButton.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
			})
			self.doSpeechRecognize()
			print("UIGestureRecognizerStateBegan.")
			microphoneButton.setImage(UIImage(named: "secondMicrophone"), for: .normal)
		}
	}
}

// MARK: - UITableViewDataSource

extension SearchViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return searchResults.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ResultTableViewCell else {
			fatalError("Invalid cell at indexPath: \(indexPath)")
		}

		let resultItem = searchResults[indexPath.row]
		cell.update(item: resultItem)
		return cell
	}

}

// MARK: - UITableViewDelegate

extension SearchViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let resultItem = searchResults[indexPath.row]
		self.openURL(urlString: resultItem.link)
	}

}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {

	func searchBarSearchButtonClicked(_ searchBarItem: UISearchBar) {

		if searchBarItem == searchBar {
			dismissKeyboard()
			if !searchResults.isEmpty {
				searchResults = []
				myTableView.reloadData()
			}
			doSearch()
		}
	}
}
// MARK: - SFSafariViewControllerDelegate

extension SearchViewController: SFSafariViewControllerDelegate {

	func openURL(urlString: String) {

		guard let url = URL(string: urlString) else { return }

		if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
			let safariViewController = SFSafariViewController(url: url)
			self.present(safariViewController, animated: true, completion: nil)
			safariViewController.delegate = self
		} else {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}

	}

	func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
		controller.dismiss(animated: true, completion: nil)
	}
}

// MARK: - DataManagerDelegate

extension SearchViewController: DataManagerDelegate {

	func dataManagerDidStartSearch() {
		activityIndicator.startAnimating()
		searchButton.setTitleForButton(NSLocalizedString("Stop", comment: ""))
	}

	func dataManagerDidFinishSearch() {
		self.activityIndicator.stopAnimating()
		self.searchButton.setTitleForButton(NSLocalizedString("Google Search", comment: ""))
	}
}
