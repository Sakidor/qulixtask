//
//  ResultItem.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/29/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation

struct ResultItem {
	var link: String
}
