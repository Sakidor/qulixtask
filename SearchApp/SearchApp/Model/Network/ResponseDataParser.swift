//
//  ResponseDataParser.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/29/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation

class ResponseDataParser {

	static var shared = ResponseDataParser()
	private init() {}

	typealias JSONDictionary = [String: Any]

	func parseSearchResponse(_ data: Data, completion: @escaping (Result<[ResultItem]>) -> Void) {

		DispatchQueue.global().async {

			func performCompletion(result: Result<[ResultItem]>) {
				DispatchQueue.main.async {
					completion(result)
				}
			}

			var response: JSONDictionary?
			var links = [ResultItem]()

			do {
				response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
			} catch {
				performCompletion(result: .mistake("JSONSerialization error"))
				return
			}

			guard let items = response!["items"] as? [Any] else {
				performCompletion(result: .mistake("Dictionary does not contain results key"))
				return
			}

			items.forEach {
				if let item = $0 as? JSONDictionary,
					let link = item["link"] as? String {
					let finalLink = ResultItem(link: link)
					links.append(finalLink)
				}
			}

			performCompletion(result: .success(links))
		}
	}
}
