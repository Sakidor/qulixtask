//
//  SpeechManager.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 2/5/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation
import Speech

class SpeechManager {

	static var shared = SpeechManager()
	private init() {}

	let audioEngine = AVAudioEngine()
	let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale(identifier: "ru-RU"))
	let request = SFSpeechAudioBufferRecognitionRequest()
	var recognitionTask: SFSpeechRecognitionTask?

	func recordAndRecognizeSpeech(completion: @escaping (String) -> Void) {

		let node = audioEngine.inputNode
		let recordingFormat = node.outputFormat(forBus: 0)
		node.removeTap(onBus: 0)
		node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
			self.request.append(buffer)
		}

		audioEngine.prepare()
		do {
			try audioEngine.start()
		} catch {
			return print(error)
		}

		guard let myRecognizer = SFSpeechRecognizer() else { return }

		if !myRecognizer.isAvailable { return }

		recognitionTask = speechRecognizer?.recognitionTask(with: request) { result, error in
			if let result = result {
				let bestString = result.bestTranscription.formattedString
				completion(bestString)
			} else if let error = error {
				print(error)
			}
		}
	}

	func stopSpeechRecognizing() {
		audioEngine.stop()
		request.endAudio()
		recognitionTask?.cancel()
	}

}
