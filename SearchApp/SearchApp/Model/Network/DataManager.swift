//
//  DataManager.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/29/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation

class DataManager {

	static var shared = DataManager()

	weak var delegate: DataManagerDelegate?
	var isSearchInProgress: Bool = false {
		didSet {
			if isSearchInProgress {
				delegate?.dataManagerDidStartSearch()
			} else {
				delegate?.dataManagerDidFinishSearch()
			}
		}
	}

	private let defaultSession = URLSession(configuration: .default)
	private var dataTask: URLSessionDataTask?

	private init () {}

	func performSearch (keyWord: String, completionHandler: @escaping (Result<[ResultItem]>) -> Void) {

		stopSearch()
		isSearchInProgress = true

		func performCompletion(result: Result<[ResultItem]>) {
			DispatchQueue.main.async {
				completionHandler(result)
			}
		}

		guard let keyWord = keyWord.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }

		let urlStr = "https://www.googleapis.com/customsearch/v1?q=\(keyWord)&cx=002059456650845357171:lj0hmzldhws&key=AIzaSyA3s1fy5qAOelwYMrS0Z6sWiG_LODqsiSU"

		guard let url = URL(string: urlStr) else { return performCompletion(result: .mistake("Invalid Url")) }

		dataTask = defaultSession.dataTask(with: url) { data, response, error in
			if let error = error, !(error._code == NSURLErrorCancelled) {
				performCompletion(result: .mistake(error.localizedDescription))
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				ResponseDataParser.shared.parseSearchResponse(data, completion: { (result) in
					performCompletion(result: result)
				})
			}
		}

		dataTask?.resume()
	}

	func stopSearch() {
		dataTask?.cancel()
		dataTask = nil
		isSearchInProgress = false
	}

}

enum Result<T> {
	case success(T)
	case mistake(String)
}

protocol DataManagerDelegate: class {

	func dataManagerDidStartSearch()
	func dataManagerDidFinishSearch()
}
