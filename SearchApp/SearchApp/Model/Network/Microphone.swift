//
//  Microphone.swift
//  SearchApp
//
//  Created by Pavel Polhovskiy on 1/31/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation
import Speech

class Microphone {

	let audioEngine = AVAudioEngine()
	let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
	let request = SFSpeechAudioBufferRecognitionRequest()
	var recognitionTask: SFSpeechRecognitionTask?
}
